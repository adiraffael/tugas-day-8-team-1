const saldo = 5000;
const PIN = 'Salah'; // choose 'Benar' or 'Salah'
const bahasa = 'Inggris'; // choose 'Indonesia' or 'Inggris'
const transaksi = 'Tunai'; // choose 'Tunai' or 'Transfer'
const cetak = 'Cetak'; // choose 'Cetak' or 'Tidak'


function pergiKeATM(){
    console.log('Pergi ke ATM terdekat.');
}

function gotoATM(){
    console.log('Go to the nearest ATM.');
}

function masukanKartuDebit(){
    console.log('Masukkan kartu debit anda.');
}

function inputyourDebit(){
    console.log('Please, input your debit.');
}

function bahasaIndonesia(){
        console.log('Anda memilih bahasa Indonesia');
        console.log('Masukkan PIN anda');
}

function bahasaInggris(){
        console.log('You choose English');
        console.log('Enter your PIN');
}

function masukanPIN(){
    if (PIN==='Benar'){
        console.log('PIN cocok');
        console.log('Silahkan pilih jenis transaksi!');
        return true;
    }
    console.log('PIN yang anda masukan salah!');
    console.log('Masukkan PIN anda');
    console.log('PIN cocok');
    console.log('Silahkan pilih jenis transaksi!');
    return false;
}

function enterPIN(){
    if (PIN==='Benar'){
        console.log('PIN valid');
        console.log('Choose your transaction type!');
        return true;
    }
    console.log('your PIN incorrect!');
    console.log('Enter your PIN');
    console.log('PIN valid');
    console.log('Choose your transaction type!');
    return false;
}

function pilihJenisTransaksi(){
    if (transaksi==='Tunai'){
        tarikTunai();
        return true;
    }
    transfer();
    return true;
}

function chooseTransaction(){
    if (transaksi==='Tunai'){
        withdraw();
        return true;
    }
    transferEng();
    return true;
}

function cekSaldo(){
    if(saldo >= 5000){
        console.log('Transaksi Berhasil');
        cetakBukti();
        return true;
    }
    console.log('Saldo kurang');
    return false;
}

function checkingSaldo(){
    if(saldo >= 5000){
        console.log('Transaction Success');
        printReceipt();
        return true;
    }
    console.log('Insufficient money!');
    return false;
}

function tarikTunai(){
    console.log('Anda memilih tarik tunai');
    console.log('Masukan nominal');
    cekSaldo();
}

function withdraw(){
    console.log('you choose withdraw');
    console.log('Input nominal')
    checkingSaldo();
}

function transfer(){
    console.log('Anda memilih transfer');
    console.log('Masukan nomor rekening tujuan.');
    console.log('Masukan nominal transfer')
    cekSaldo();
}

function transferEng(){
    console.log('you choose transfer');
    console.log('Input your destinate account number');
    console.log('Input nominal');
    checkingSaldo();
}

function ambilUang(){
    console.log('Anda mengambil uang anda');
}

function takeMoney(){
    console.log('Please, take your money');
}

function cetakBukti(){
    if (cetak==='Cetak'){
        console.log('Anda ingin mencetak resi');
        console.log('Resi tercetak, ambil resi anda.')
        return true;
    }
    console.log('Anda tidak ingin mencetak bukti');
    return true;
}

function printReceipt(){
    if (cetak==='Cetak'){
        console.log('You want to print receipt');
        console.log('Receipt printed, take your receipt.')
        return true;
    }
    console.log('You dont want to print receipt');
    return true;
}

function keluar(){
    console.log('Silahkan ambil kartu anda kembali.');
}

function logout(){
    console.log('Please pick your card.');
}

function indonesia(){
    pergiKeATM();
    masukanKartuDebit();
    bahasaIndonesia();
    masukanPIN();
    pilihJenisTransaksi();
    keluar();
}

function inggris(){
    gotoATM();
    inputyourDebit();
    bahasaInggris();
    enterPIN();
    chooseTransaction();
    logout();
}

function start() {
    if (bahasa == 'Indonesia') {
        return indonesia()
    } else {
        return inggris()
    }
}
start();